namespace Structure;

public partial class DemoStackLayout : ContentPage
{
	public DemoStackLayout()
	{
		InitializeComponent();

		if (DeviceInfo.Platform == DevicePlatform.Android)
		{
			// code spécifique android
		}

		if( DeviceInfo.Idiom == DeviceIdiom.Desktop)
		{
			// code spécifique bureau
		}
	}
}