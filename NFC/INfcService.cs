﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NFC;

public interface INfcService
{
    Task<byte[]> ReadAsync();

    Task<bool> SendAsync(byte[] data);

    Task<bool> OpenNfcSettings() => Task.FromResult(true);
}
