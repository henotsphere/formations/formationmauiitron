﻿using System.Text;

namespace NFC
{
    public partial class MainPage : ContentPage
    {
        readonly INfcService _nfcService;

        public MainPage(INfcService nfcService)
        {
            InitializeComponent();

            _nfcService = nfcService;

            btnRead.Clicked += BtnRead_Clicked;
            btnSend.Clicked += BtnSend_Clicked;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await _nfcService.OpenNfcSettings();
        }

        private async void BtnSend_Clicked(object? sender, EventArgs e)
        {
            var data = Encoding.UTF8.GetBytes(edtData.Text);

            var isSuccess = await _nfcService.SendAsync(data);

            if (isSuccess) await DisplayAlert("Tag NFC", "Ecriture réussie", "Ok");
        }

        private async void BtnRead_Clicked(object? sender, EventArgs e)
        {
            var data = await _nfcService.ReadAsync();

            lblRead.Text = Encoding.UTF8.GetString(data);
        }
    }

}
