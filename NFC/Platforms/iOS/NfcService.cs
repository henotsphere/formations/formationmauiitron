﻿using CoreNFC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIKit;
using Application = Microsoft.Maui.Controls.Application;

namespace NFC;

internal class NfcService : INfcService
{
    public Task<byte[]> ReadAsync()
    {
        throw new NotImplementedException();
    }

    public async Task<bool> SendAsync(byte[] data)
    {
        var isNfcAvailable = UIDevice.CurrentDevice.CheckSystemVersion(11, 0);
        if (!(isNfcAvailable && NFCNdefReaderSession.ReadingAvailable))
        {
            await Application.Current!.MainPage!.DisplayAlert("Erreur", "NFC non supporté", "Ok");
            return false;
        }

        return await MainThread.InvokeOnMainThreadAsync(async () =>
        {
            try
            {
                var sessionDelegate = new NfcSessionDelegate(data);
                var session = new NFCNdefReaderSession(sessionDelegate, null, true);

                session.BeginSession();

                return true;
            }
            catch
            {
                await Application.Current!.MainPage!.DisplayAlert("Erreur", "Impossible de créer une session NFC", "Ok");
                return false;
            }
        });
    }
}
