﻿using CoreNFC;
using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIKit;

namespace NFC;

internal class NfcSessionDelegate(byte[] data) : NFCNdefReaderSessionDelegate
{
    readonly byte[] _data = data;

    public override void DidDetectTags(NFCNdefReaderSession session, INFCNdefTag[] tags)
    {
        MainThread.BeginInvokeOnMainThread(() =>
        {
            session.AlertMessage = "Tag détecté";

            try
            {
                if (tags.Length != 1)
                {
                    session.InvalidateSession("Impossible d'écrire sur plusieurs tags en même temps");
                    return;
                }

                var tag = tags.First();
                session.ConnectToTag(tag, (error) =>
                {
                    if (error is not null)
                    {
                        session.InvalidateSession("Impossible de se connecter au tag");
                        return;
                    }
                });

                tag.QueryNdefStatus((status, capacity, error) =>
                {
                    if (error is not null)
                    {
                        session.InvalidateSession("Impossible de récupérer le statut du tag");
                    }

                    switch (status)
                    {
                        case NFCNdefStatus.NotSupported:
                            session.InvalidateSession("Tag non supporté");
                            break;
                        case NFCNdefStatus.ReadOnly:
                            session.InvalidateSession("Tag en lecture seule");
                            break;
                        case NFCNdefStatus.ReadWrite:
                            WriteToTag(session, tag);
                            break;
                        default:
                            session.InvalidateSession("statut du tag inconnu");
                            break;
                    }
                });
            }
            catch (Exception ex)
            {
                session.InvalidateSession(ex.Message);
            }
        });
    }

    public override void DidInvalidate(NFCNdefReaderSession session, NSError error)
    {
        if (error.Code == (int)NFCReaderError.ReaderSessionInvalidationErrorSessionTimeout)
        {
            MainThread.BeginInvokeOnMainThread(async () =>
            {
                await Application.Current!.MainPage!.DisplayAlert("Erreur", "La recherche de tag a expiré", "Ok");
            });
        }
    }

    private void WriteToTag(NFCNdefReaderSession session, INFCNdefTag tag)
    {
        var isNfcWriteAvailable = UIDevice.CurrentDevice.CheckSystemVersion(13, 0);
        if (!isNfcWriteAvailable)
        {
            session.InvalidateSession("Impossible d'écrire sur ce tag");
            return;
        }

        var text = Encoding.UTF8.GetString(data);
        var payload = NFCNdefPayload.CreateWellKnownTypePayload(text);
        var ndefMessage = new NFCNdefMessage([payload!]);

        tag.WriteNdef(ndefMessage, (error) =>
        {
            if (error is not null)
                session.InvalidateSession("Une erreur s'est produite lors de la transmission; appareil déplacé");
            else
            {
                session.AlertMessage = "Ecriture réussie";
                session.InvalidateSession();
            }
        });
    }
}
