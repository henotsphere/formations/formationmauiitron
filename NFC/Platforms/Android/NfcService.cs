﻿using Android.App;
using Android.Content;
using Android.Nfc;
using Android.Nfc.Tech;
using Android.OS;
using Android.Util;
using Android.Provider;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Xamarin.Google.Crypto.Tink.Subtle;
using Application = Microsoft.Maui.Controls.Application;

namespace NFC;

internal class NfcService : INfcService, IDisposable
{
    readonly MainActivity? _mainActivity;
    readonly Lazy<NfcAdapter?> _lazyAdapter;
    readonly PendingIntent? _pendingIntent;
    readonly IntentFilter[] _intentFilters;
    readonly string[][]? _techList;

    NfcAdapter? Adapter => _lazyAdapter.Value;

    Tag? DetectedTag { get; set; }

    public NfcService()
    {
        _mainActivity = (MainActivity?)Platform.CurrentActivity;

        _lazyAdapter = new(() => NfcAdapter.GetDefaultAdapter(_mainActivity));

        _intentFilters = [
            new(NfcAdapter.ActionTagDiscovered),
            new(NfcAdapter.ActionTechDiscovered),
            new(NfcAdapter.ActionNdefDiscovered)
        ];

        foreach (var intentFilter in _intentFilters) intentFilter.AddCategory(Intent.CategoryDefault);

        _techList = GetNfcTechFilters();

        var intent = new Intent(_mainActivity, _mainActivity?.Class)
            .AddFlags(ActivityFlags.SingleTop);
        var pendingIntentFlags = Build.VERSION.SdkInt >= BuildVersionCodes.S ?
            PendingIntentFlags.Mutable :
            0;
        _pendingIntent = PendingIntent.GetActivity(_mainActivity, 0, intent, pendingIntentFlags);

        Platform.ActivityStateChanged += Platform_ActivityStateChanged;
    }

    private void Platform_ActivityStateChanged(object? sender, ActivityStateChangedEventArgs e)
    {
        switch (e.State)
        {
            case ActivityState.Resumed:
                EnableForegroundDispatch();
                break;
            case ActivityState.Paused:
                DisableForegroundDispatch();
                break;
            default:
                break;
        }
    }

    public void EnableForegroundDispatch()
    {
        Adapter?.EnableForegroundDispatch(_mainActivity, _pendingIntent, _intentFilters, _techList);
    }

    public void DisableForegroundDispatch()
    {
        Adapter?.DisableForegroundDispatch(_mainActivity);
    }

    public async Task<byte[]> ReadAsync()
    {
        Ndef? ndef = null;

        try
        {
            DetectedTag ??= await GetDetectedTagAsync();

            ndef = Ndef.Get(DetectedTag);
            if (ndef is null) return [];

            if (!ndef.IsConnected) await ndef.ConnectAsync();

            return ReadFromTag(ndef);
        }
        catch (IOException)
        {
            await Application.Current!.MainPage!.DisplayAlert("Erreur", "Une erreur s'est produite pendant la transmission ; l'appareil peut avoir été déplacé", "Ok");
            return [];
        }
        catch (Exception ex)
        {
            await Application.Current!.MainPage!.DisplayAlert("Erreur", ex.Message, "Ok");
            return [];
        }
        finally
        {
            if (ndef?.IsConnected ?? false) ndef.Close();
            DetectedTag = null;
        }
    }

    byte[] ReadFromTag(Ndef ndef)
    {
        var ndefMessage = ndef.NdefMessage;
        if (ndefMessage is null) return [];

        var record = ndefMessage.GetRecords()?
            .FirstOrDefault(r => r.Tnf == NdefRecord.TnfWellKnown && r.GetTypeInfo()!.SequenceEqual([.. NdefRecord.RtdText!]));

        return record?.GetPayload() ?? []; //Pas de record correspondant
    }

    public async Task<bool> SendAsync(byte[] data)
    {
        Ndef? ndef = null;

        try
        {
            DetectedTag ??= await GetDetectedTagAsync();

            ndef = Ndef.Get(DetectedTag);
            if (ndef is null) return false;

            if (!ndef.IsWritable)
            {
                await Application.Current!.MainPage!.DisplayAlert("Erreur", "Le tag est en lecture seule", "Ok");
                return false;
            }

            if (!ndef.IsConnected) await ndef.ConnectAsync();

            await WriteToTagAsync(ndef, data);

            return true;
        }
        catch (IOException)
        {
            await Application.Current!.MainPage!.DisplayAlert("Erreur", "Une erreur s'est produite pendant la transmission ; l'appareil peut avoir été déplacé", "Ok");
            return false;
        }
        catch (Exception ex)
        {
            await Application.Current!.MainPage!.DisplayAlert("Erreur", ex.Message, "Ok");
            return false;
        }
        finally
        {
            if (ndef?.IsConnected ?? false) ndef.Close();
            DetectedTag = null;
        }
    }

    private async Task WriteToTagAsync(Ndef ndef, byte[] data)
    {
        var ndefRecord = new NdefRecord(NdefRecord.TnfWellKnown, NdefRecord.RtdText!.ToArray(), [], data);
        var ndefMessage = new NdefMessage([ndefRecord]);

        await ndef.WriteNdefMessageAsync(ndefMessage);
    }

    public async Task<bool> OpenNfcSettings()
    {
        if (Adapter is null)
        {
            await Application.Current!.MainPage!.DisplayAlert("Erreur", "NFC non supporté", "Ok");
            return false;
        }

        if (!Adapter.IsEnabled)
        {
            await Application.Current!.MainPage!.DisplayAlert("Désactivé", "NFC désactivé", "Ok");

            var intent = new Intent(Settings.ActionNfcSettings);

            _mainActivity?.StartActivity(intent);
        }

        return true;
    }

    static string[][]? GetNfcTechFilters()
    {
        List<List<string>> techList = [];
        int currentList = -1;

        using var reader = Platform.CurrentActivity?.Resources?.GetXml(Resource.Xml.nfc_tech_filter);

        if (reader is null) return null;

        reader.MoveToContent();

        while (reader.Read())
        {
            switch (reader.NodeType)
            {
                case XmlNodeType.Element:
                    if (reader.Name == "tech-list")
                    {
                        techList.Add([]);
                        currentList++;
                    }
                    break;
                case XmlNodeType.Text:
                    techList[currentList].Add(reader.Value);
                    break;
                default:
                    break;
            }
        }

        return techList.Select(x => x.ToArray()).ToArray();
    }

    async Task<Tag?> GetDetectedTagAsync()
    {
        if (_mainActivity is null) return null;

        _mainActivity.NfcTag = new TaskCompletionSource<Tag>();

        return await _mainActivity.NfcTag.Task;
    }

    public void Dispose()
    {
        Platform.ActivityStateChanged -= Platform_ActivityStateChanged;
    }
}
