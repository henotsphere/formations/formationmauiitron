﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Nfc;
using AndroidX.Core.Content;

namespace NFC;

[MetaData(NfcAdapter.ActionTechDiscovered, Resource = "@xml/nfc_tech_filter")]
[IntentFilter([NfcAdapter.ActionTechDiscovered], Categories = [Intent.CategoryDefault], DataMimeType = "text/plain")]
[IntentFilter([NfcAdapter.ActionNdefDiscovered], Categories = [Intent.CategoryDefault], DataMimeType = "text/plain")]
[IntentFilter([NfcAdapter.ActionTagDiscovered], Categories = [Intent.CategoryDefault], DataMimeType = "text/plain")]
[Activity(Theme = "@style/Maui.SplashTheme", MainLauncher = true, LaunchMode = LaunchMode.SingleTop, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize | ConfigChanges.Density)]
public class MainActivity : MauiAppCompatActivity
{
    public TaskCompletionSource<Tag>? NfcTag { get; set; }

    protected override void OnNewIntent(Intent? intent)
    {
        base.OnNewIntent(intent);

        switch (intent?.Action)
        {
            case NfcAdapter.ActionTechDiscovered:
            case NfcAdapter.ActionNdefDiscovered:
            case NfcAdapter.ActionTagDiscovered:
                ReadNfcIntent(intent);
                break;
            default:
                break;
        }
    }

    private void ReadNfcIntent(Intent intent)
    {
        Tag? tag;

        if (Build.VERSION.SdkInt >= BuildVersionCodes.Tiramisu)
        {
            var javaType = Java.Lang.Class.FromType(typeof(Tag));
#pragma warning disable CA1416
            tag = (Tag?)intent.GetParcelableExtra(NfcAdapter.ExtraTag, javaType);
#pragma warning restore CA1416
        }
        else
        {
#pragma warning disable CA1422
            tag = (Tag?)intent.GetParcelableExtra(NfcAdapter.ExtraTag);
#pragma warning restore CA1422
        }

        //OU

        //var javaType = Java.Lang.Class.FromType(typeof(Tag));
        //tag = (Tag?)IntentCompat.GetParcelableExtra(intent, NfcAdapter.ExtraTag, javaType);

        if (tag is null) return;

        NfcTag?.TrySetResult(tag);
    }
}
