﻿using System.ComponentModel;
using XamarinForms.ViewModels;
using Microsoft.Maui.Controls;
using Microsoft.Maui;

namespace XamarinForms.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}