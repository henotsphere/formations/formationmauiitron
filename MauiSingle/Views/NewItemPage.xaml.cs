﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.Maui.Controls.Xaml;

using XamarinForms.Models;
using XamarinForms.ViewModels;
using Microsoft.Maui.Controls;
using Microsoft.Maui;

namespace XamarinForms.Views
{
    public partial class NewItemPage : ContentPage
    {
        public Item Item { get; set; }

        public NewItemPage()
        {
            InitializeComponent();
            BindingContext = new NewItemViewModel();
        }
    }
}