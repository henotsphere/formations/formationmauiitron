﻿using Shiny.BluetoothLE;
using System.Diagnostics;
using System.Text;

namespace Bluetooth
{
    public partial class MainPage : ContentPage
    {
        readonly IBleManager _bleManager;

        IDisposable? _scanSub;
        IDisposable? _notifySub;

        int conPeriphCount = 0;

        public MainPage(IBleManager bleManager)
        {
            InitializeComponent();

            _bleManager = bleManager;

            swScan.Toggled += SwScan_Toggled;
            btnWrite.Clicked += BtnWrite_Clicked; ;
            swSub.Toggled += SwSub_Toggled;
        }

        private async void SwSub_Toggled(object? sender, ToggledEventArgs e)
        {
            if (e.Value) await Subscribe();
            else Unsubscribe();
        }

        private async void BtnWrite_Clicked(object? sender, EventArgs e)
        {
            var data = Encoding.UTF8.GetBytes(entWrite.Text);

            await Write(data);
        }

        private async void SwScan_Toggled(object? sender, ToggledEventArgs e)
        {
            if (e.Value)
            {
                if (!await CheckBtSettings())
                {
                    // on annule
                    return;
                }

                ScanAndConnect();
            }
            else
            {
                StopScan();

                btnWrite.IsEnabled = true;
                entWrite.IsEnabled = true;
                swSub.IsEnabled = true;
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            lblScan.Text = "En attente de scan";
            lblSubRead.Text = "...";

            btnWrite.IsEnabled = false;
            entWrite.IsEnabled = false;
            swSub.IsEnabled = false;

            conPeriphCount = 0;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            swScan.IsToggled = false; //StopScan
            swSub.IsToggled = false; //Unsubscribe

            foreach (var periph in _bleManager.GetConnectedPeripherals())
                periph.CancelConnection();
        }

        async Task<bool> CheckBtSettings()
        {
            var access = await _bleManager.RequestAccessAsync();

            if (access != Shiny.AccessState.Available)
            {
                await DisplayAlert("Erreur", "Bluetooth non disponible", "Ok");
                Debug.Print("BT AccessState : {0}", access);

                return false;
            }

            return true;
        }

        void ScanAndConnect()
        {
            if (_bleManager.IsScanning) return;

            _scanSub = _bleManager.ScanForUniquePeripherals(new ScanConfig(MauiProgram.ServiceUuid))
                .Subscribe(async (peripheral) =>
                {
                    await peripheral.ConnectAsync(new ConnectionConfig(false), timeout: TimeSpan.FromSeconds(5));

                    MainThread.BeginInvokeOnMainThread(() =>
                    {
                        lblScan.Text = $"Connecté à {++conPeriphCount} périphérique(s)";
                    });

                    Debug.Print("BT Connecté au périphérique : {0}", peripheral.Name);
                });
        }

        void StopScan()
        {
            _scanSub?.Dispose();
            _scanSub = null;
        }

        async Task<byte[]?> Read()
        {
            var peripheral = _bleManager.GetConnectedPeripherals().FirstOrDefault();
            if (peripheral is null)
            {
                await DisplayAlert("Erreur", "Aucun appareil connecté. Effectuez un scan", "Ok");
                return null;
            }

            var res = await peripheral.ReadCharacteristicAsync(MauiProgram.ServiceUuid, MauiProgram.ReadCharacteristic);

            if (res is null)
            {
                await DisplayAlert("Erreur", "Impossible de récupérer les données", "Ok");
                return null;
            }

            Debug.Print("BT Données récupérées");

            return res.Data;
        }

        async Task Write(byte[] data)
        {
            var peripheral = _bleManager.GetConnectedPeripherals().FirstOrDefault();
            if (peripheral is null)
            {
                await DisplayAlert("Erreur", "Aucun appareil connecté. Effectuez un scan", "Ok");
                return;
            }

            var res = await peripheral.WriteCharacteristicAsync(MauiProgram.ServiceUuid, MauiProgram.WriteCharacteristic, data, withResponse: false);

            if (res is null)
                await DisplayAlert("Erreur", "Impossible d'envoyer les données", "Ok");

            Debug.Print("BT Données envoyées");
        }

        async Task Subscribe()
        {
            if (_notifySub is not null) return;

            var peripheral = _bleManager.GetConnectedPeripherals().FirstOrDefault();
            if (peripheral is null)
            {
                await DisplayAlert("Erreur", "Aucun appareil connecté. Effectuez un scan", "Ok");
                return;
            }

            _notifySub = peripheral.NotifyCharacteristic(MauiProgram.ServiceUuid, MauiProgram.NotifyCharacteristic)
                .Subscribe(res =>
                {
                    MainThread.BeginInvokeOnMainThread(() =>
                    {
                        lblSubRead.Text = Encoding.UTF8.GetString(res.Data!);
                    });

                    Debug.Print("BT Notification - Données reçues");
                });
        }

        void Unsubscribe()
        {
            _notifySub?.Dispose();
            _notifySub = null;
        }
    }
}
