﻿using Microsoft.Extensions.Logging;
using Shiny;

namespace Bluetooth
{
    public static class MauiProgram
    {
        public const string ServiceUuid = "0000ffe0-0000-1000-8000-00805F9B34FB";
        public const string WriteCharacteristic = "0000ffe1-0000-1000-8000-00805F9B34FB";
        public const string ReadCharacteristic = "0000ffe1-0000-1000-8000-00805F9B34FB";
        public const string NotifyCharacteristic = "0000ffe1-0000-1000-8000-00805F9B34FB";

        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                .UseShiny()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                    fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
                });

#if DEBUG
    		builder.Logging.AddDebug();
#endif

            builder.Services.AddBluetoothLE();

            builder.Services.AddSingleton<MainPage>();

            return builder.Build();
        }
    }
}
