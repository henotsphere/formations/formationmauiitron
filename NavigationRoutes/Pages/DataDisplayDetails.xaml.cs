namespace NavigationRoutes.Pages;

// Nom du param�tre sensible � la casse
[QueryProperty(nameof(Objet), "objet")]
public partial class DataDisplayDetails : ContentPage
{
	// prop r�ceptrice du param�tre de requ�te
	string? objet;
	public string? Objet
	{
		get => objet;
		set
		{
			objet = value;
			lblValueObjet.Text = value;
		}
	}

	public DataDisplayDetails()
	{
		InitializeComponent();
	}
}