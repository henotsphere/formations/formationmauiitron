namespace NavigationRoutes.Pages;

public partial class DataDisplay : ContentPage
{
	public DataDisplay()
	{
		InitializeComponent();

		// URI relative vers DataDisplayDetails car route = //base/DataDisplay selon AppShell
		btnTable.Clicked += async (s, e) => await Shell.Current.GoToAsync("Details?objet=La table");
        btnBureau.Clicked += async (s, e) => await Shell.Current.GoToAsync("Details?objet=Un grand bureau");
        btnBouteille.Clicked += async (s, e) => await Shell.Current.GoToAsync("Details?objet=Une bouteille d'eau gazeuze");
    }
}