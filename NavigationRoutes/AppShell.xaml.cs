﻿using NavigationRoutes.Pages;

namespace NavigationRoutes
{
    public partial class AppShell : Shell
    {
        public AppShell()
        {
            InitializeComponent();

            Routing.RegisterRoute("//base/DataDisplay/Details", typeof(DataDisplayDetails));
        }
    }
}
