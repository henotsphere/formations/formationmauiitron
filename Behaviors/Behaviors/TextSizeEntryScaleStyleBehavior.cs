﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Behaviors.Behaviors;

class TextSizeEntryScaleStyleBehavior : Behavior<Entry>
{
    public static readonly BindableProperty AttachBehaviorProperty =
        BindableProperty.CreateAttached("AttachBehavior", typeof(bool), typeof(TextSizeEntryScaleStyleBehavior), false, propertyChanged: OnAttachBehaviorChanged);

    public static bool GetAttachBehavior(BindableObject view) => (bool)view.GetValue(AttachBehaviorProperty);

    public static void SetAttachBehavior(BindableObject view, bool value) => view.SetValue(AttachBehaviorProperty, value);

    private static void OnAttachBehaviorChanged(BindableObject bindable, object oldValue, object newValue)
    {
        if (bindable is not Entry entry) return;

        bool attachBehavior = (bool)newValue;
        if (attachBehavior) entry.Behaviors.Add(new TextSizeEntryScaleStyleBehavior());
        else
        {
            var behavior = entry.Behaviors.FirstOrDefault(b => b is TextSizeEntryScaleStyleBehavior);
            if (behavior is not null) entry.Behaviors.Remove(behavior);
        }
    }

    protected override void OnAttachedTo(Entry bindable)
    {
        bindable.TextChanged += Entry_TextChanged;
    }

    private void Entry_TextChanged(object? sender, TextChangedEventArgs e)
    {
        var entry = (Entry)sender!;

        entry.Scale = 0.7 + (entry.Text.Length * 0.1);
    }

    protected override void OnDetachingFrom(Entry bindable)
    {
        bindable.TextChanged -= Entry_TextChanged;
    }
}
