﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Behaviors.Behaviors;

class TextSizeEntryScaleBehavior : Behavior<Entry>
{
    protected override void OnAttachedTo(Entry bindable)
    {
        bindable.TextChanged += Entry_TextChanged;
    }

    private void Entry_TextChanged(object? sender, TextChangedEventArgs e)
    {
        var entry = (Entry)sender!;

        entry.Scale = 0.7 + (entry.Text.Length * 0.1);
    }

    protected override void OnDetachingFrom(Entry bindable)
    {
        bindable.TextChanged -= Entry_TextChanged;
    }
}
