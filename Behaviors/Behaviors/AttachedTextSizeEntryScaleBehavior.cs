﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Behaviors.Behaviors;

static class AttachedTextSizeEntryScaleBehavior
{
    public static readonly BindableProperty AttachBehaviorProperty =
        BindableProperty.CreateAttached("AttachBehavior", typeof(bool), typeof(AttachedTextSizeEntryScaleBehavior), false, propertyChanged: OnAttachBehaviorChanged);

    public static bool GetAttachBehavior(BindableObject view) => (bool)view.GetValue(AttachBehaviorProperty);

    public static void SetAttachBehavior(BindableObject view, bool value) => view.SetValue(AttachBehaviorProperty, value);

    static void OnAttachBehaviorChanged(BindableObject bindable, object oldValue, object newValue)
    {
        if (bindable is not Entry entry) return;

        /* On peut soit:
         * - affecter une propriété du contrôle directement
         * - affecter une propriété du contrôle en fonction d'un évènement du contrôle (= trigger)
         * Ici on utilise un gestionnaire qu'on inscrit pour un event si le comportement est attaché, et inversement
         */

        bool attachBehavior = (bool)newValue;
        if (attachBehavior) entry.TextChanged += Entry_TextChanged;
        else entry.TextChanged -= Entry_TextChanged;
    }

    private static void Entry_TextChanged(object? sender, TextChangedEventArgs e)
    {
        var entry = (Entry)sender!;

        entry.Scale = 0.7 + (entry.Text.Length * 0.1);
    }
}
