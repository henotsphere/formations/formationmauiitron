﻿using Behaviors.Behaviors;

namespace Behaviors
{
    public partial class MainPage : ContentPage
    {
        int count = 0;

        public MainPage()
        {
            InitializeComponent();
        }

        private void OnCounterClicked(object sender, EventArgs e)
        {
            count++;

            if (count == 1)
                CounterBtn.Text = $"Clicked {count} time";
            else
                CounterBtn.Text = $"Clicked {count} times";

            SemanticScreenReader.Announce(CounterBtn.Text);

            var behavior = entry1.Behaviors.FirstOrDefault(b => b is TextSizeEntryScaleBehavior);
            if (behavior is not null) entry1.Behaviors.Remove(behavior);
        }
    }

}
