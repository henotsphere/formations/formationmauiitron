﻿using Android.Content.Res;
using Android.Widget;
using Microsoft.Maui.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Behaviors.Behaviors;

partial class PasswordStrengthColorBehavior : PlatformBehavior<Entry, EditText>
{
    protected override void OnAttachedTo(Entry bindable, EditText platformView)
    {
        base.OnAttachedTo(bindable, platformView);

        if (bindable is null) return;

        platformView.TextChanged += PlatformView_TextChanged;
    }

    private void PlatformView_TextChanged(object? sender, global::Android.Text.TextChangedEventArgs e)
    {
        var editText = (EditText)sender!;

        editText.BackgroundTintList = editText.Length() switch
        {
            >= 8 and < 16 => ColorStateList.ValueOf(MediumColor.ToPlatform()),
            >= 16 => ColorStateList.ValueOf(StrongColor.ToPlatform()),
            _ => ColorStateList.ValueOf(WeakColor.ToPlatform())
        };
    }

    protected override void OnDetachedFrom(Entry bindable, EditText platformView)
    {
        base.OnDetachedFrom(bindable, platformView);

        if (bindable is null) return;

        platformView.TextChanged -= PlatformView_TextChanged;
    }
}
